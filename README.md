# Commerce PostFinance

Provides Commerce integration for the PostFinance checkout payment api.

This payment gateway redirects to [PostFinance](https://www.postfinance.ch)
where the user is able to pay with all activated payment methods, such as
_PostFinance Card_, _Visa, Mastercard_, _Twint_ and many more.

## Installation

Install with Composer:

```
composer require drupal/commerce_postfinance_checkout:^1.0
```

Enable the module in Drupal and create a new payment gateway of type
*PostFinance*.

