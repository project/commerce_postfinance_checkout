<?php

namespace Drupal\Tests\commerce_postfinance_checkout\Kernel;

use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_postfinance_checkout\PostFinanceServiceFactory;
use Drupal\commerce_price\Price;
use PostFinanceCheckout\Sdk\Model\Refund;
use PostFinanceCheckout\Sdk\Model\RefundState;
use PostFinanceCheckout\Sdk\Model\Transaction;
use PostFinanceCheckout\Sdk\Model\TransactionState;
use PostFinanceCheckout\Sdk\Service\RefundService;
use PostFinanceCheckout\Sdk\Service\TransactionService;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\commerce_postfinance_checkout\Plugin\Commerce\PaymentGateway\PostFinanceCheckout
 *
 * @group commerce_postfinance_checkout
 */
class CheckoutGatewayTest extends CheckoutKernelTestBase {

  /**
   * @covers ::onReturn
   */
  public function testOnReturnComplete() {
    $transaction = new Transaction();
    $transaction->setState(TransactionState::COMPLETED);

    $transaction_service = $this->prophesize(TransactionService::class);
    $transaction_service
      ->read($this->spaceId, 123)
      ->willReturn($transaction);

    $service_factory = $this->prophesize(PostFinanceServiceFactory::class);
    $service_factory
      ->getTransactionService($this->userId, $this->secret)
      ->willReturn($transaction_service->reveal());

    $this->container->set('commerce_postfinance_checkout.service_factory', $service_factory->reveal());

    $gateway = $this->getGateway();

    // Create an existing, pending payment.
    $payment = Payment::create([
      'payment_gateway' => $this->paymentGateway->id(),
      'remote_id' => 123,
      'order_id' => $this->order->id(),
      'state' => 'pending',
      'amount' => $this->order->getTotalPrice(),
    ]);
    $payment->save();

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', []);
    $gateway->onReturn($this->order, $request);

    // Process a second time, ensure that no duplicate is created.
    $gateway->onReturn($this->order, $request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals('postfinance_checkout_id', $payment->getPaymentGatewayId());
  }

  /**
   * @covers ::onReturn
   */
  public function testOnReturnVoid() {
    $transaction = new Transaction();
    $transaction->setState(TransactionState::VOIDED);

    $transaction_service = $this->prophesize(TransactionService::class);
    $transaction_service
      ->read($this->spaceId, 123)
      ->willReturn($transaction);

    $service_factory = $this->prophesize(PostFinanceServiceFactory::class);
    $service_factory
      ->getTransactionService($this->userId, $this->secret)
      ->willReturn($transaction_service->reveal());

    $this->container->set('commerce_postfinance_checkout.service_factory', $service_factory->reveal());
    $gateway = $this->getGateway();

    // Create an existing, pending payment.
    $payment = Payment::create([
      'payment_gateway' => $this->paymentGateway->id(),
      'remote_id' => 123,
      'order_id' => $this->order->id(),
      'state' => 'pending',
      'amount' => $this->order->getTotalPrice(),
    ]);
    $payment->save();

    // Create the request and call onReturn().
    $request = Request::create('', 'GET', []);
    try {
      $gateway->onReturn($this->order, $request);
    }
    catch (\Exception $e) {
      $this->assertTrue($e instanceof PaymentGatewayException);
    }

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);
  }

  /**
   * @covers ::refundPayment
   */
  public function testRefundPayment() {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = Payment::create([
      'state' => 'completed',
      'amount' => $this->order->getTotalPrice(),
      'payment_gateway' => $this->paymentGateway->id(),
      'order_id' => $this->order->id(),
      'test' => TRUE,
      'remote_id' => '123',
      'remote_state' => 'test',
      'authorized' => \Drupal::time()->getRequestTime(),
    ]);
    $payment->save();

    $trans = new Transaction();
    $trans->setId($payment->getRemoteId());

    $refund = new Refund();
    $refund->setState(RefundState::SUCCESSFUL);

    $refund_service = $this->prophesize(RefundService::class);
    $refund_service
      ->refund($this->spaceId, Argument::any())
      ->willReturn($refund);

    $service_factory = $this->prophesize(PostFinanceServiceFactory::class);
    $service_factory
      ->getRefundService($this->userId, $this->secret)
      ->willReturn($refund_service->reveal());
    $this->container->set('commerce_postfinance_checkout.service_factory', $service_factory->reveal());

    $gateway = $this->getGateway();
    $gateway->refundPayment($payment);

    $this->assertEquals('refunded', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getRefundedAmount());
    $this->assertEquals(new Price(0, 'USD'), $payment->getBalance());
    $this->assertEquals('postfinance_checkout_id', $payment->getPaymentGatewayId());
  }


  /**
   * @covers ::refundPayment
   */
  public function testRefundPaymentPartial() {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = Payment::create([
      'state' => 'completed',
      'amount' => $this->order->getTotalPrice(),
      'payment_gateway' => $this->paymentGateway->id(),
      'order_id' => $this->order->id(),
      'test' => TRUE,
      'remote_id' => '123',
      'remote_state' => 'test',
      'authorized' => \Drupal::time()->getRequestTime(),
    ]);
    $payment->save();

    $trans = new Transaction();
    $trans->setId($payment->getRemoteId());

    $refund = new Refund();
    $refund->setState(RefundState::SUCCESSFUL);

    $refund_service = $this->prophesize(RefundService::class);
    $refund_service
      ->refund($this->spaceId, Argument::any())
      ->willReturn($refund);

    $service_factory = $this->prophesize(PostFinanceServiceFactory::class);
    $service_factory
      ->getRefundService($this->userId, $this->secret)
      ->willReturn($refund_service->reveal());
    $this->container->set('commerce_postfinance_checkout.service_factory', $service_factory->reveal());

    $gateway = $this->getGateway();
    $refund_amount = new Price('123.50', 'USD');
    $gateway->refundPayment($payment, $refund_amount);

    $this->assertEquals('partially_refunded', $payment->getState()->value);
    $this->assertEquals(123, $payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $payment->getAmount());
    $this->assertEquals($refund_amount, $payment->getRefundedAmount());
    $this->assertEquals(new Price('875.5', 'USD'), $payment->getBalance());
    $this->assertEquals('postfinance_checkout_id', $payment->getPaymentGatewayId());
  }

}
