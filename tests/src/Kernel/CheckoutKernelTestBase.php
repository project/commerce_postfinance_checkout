<?php

namespace Drupal\Tests\commerce_postfinance_checkout\Kernel;

use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Base class for PostFinance kernel tests.
 */
abstract class CheckoutKernelTestBase extends CommerceKernelTestBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The payment gateway config entity.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $paymentGateway;

  /**
   * Secret.
   *
   * @var string
   */
  protected $secret = '123asdf123asdf';

  /**
   * Space id.
   *
   * @var string
   */
  protected $spaceId = '1234';

  /**
   * User id.
   *
   * @var string
   */
  protected $userId = '1234';

  protected static $modules = [
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'commerce_number_pattern',
    'commerce_order',
    'commerce_payment',
    'commerce_postfinance_checkout',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_payment');
    $this->installConfig(['commerce_order']);

    OrderItemType::create([
      'id' => 'default',
      'label' => 'Default',
      'orderType' => 'default',
    ])->save();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    // Create an order.
    $this->order = $this->entityTypeManager->getStorage('commerce_order')->create([
      'type' => 'default',
      'order_number' => '1',
      'store_id' => $this->store->id(),
      'state' => 'draft',
    ]);
    $this->order->save();

    $order_item = $this->entityTypeManager->getStorage('commerce_order_item')->create([
      'type' => 'default',
      'unit_price' => [
        'number' => '999',
        'currency_code' => 'USD',
      ],
    ]);
    $order_item->save();
    $this->order->setItems([$order_item]);
    $this->paymentGateway = PaymentGateway::create([
      'id' => 'postfinance_checkout_id',
      'plugin' => 'postfinance_checkout',
      'configuration' => [
        'space_id' => $this->spaceId,
        'user_id' => $this->userId,
        'secret' => $this->secret,
        'payment_configuration' => 'all',
        'payment_reusable' => 'ask',
      ]
    ]);
    $this->paymentGateway->save();

    $this->order->set('payment_gateway', $this->paymentGateway->id());
    $this->order->save();
  }

  /**
   * Returns the payment gateway.
   *
   * @return \Drupal\commerce_postfinance_checkout\Plugin\Commerce\PaymentGateway\PostFinanceCheckout
   *   The gateway.
   */
  protected function getGateway() {
    $configuration = [
        '_entity' => $this->paymentGateway,
      ] + $this->paymentGateway->getPluginConfiguration();
    $gateway_plugin_manager = $this->container->get('plugin.manager.commerce_payment_gateway');
    return $gateway_plugin_manager->createInstance('postfinance_checkout', $configuration);
  }

  /**
   * Returns example transaction data.
   *
   * @param string $status
   *   The payment status.
   *
   * @return array
   *   The data as returned by the API.
   */
  protected function getTransactionData(string $status = 'settled'): array {
    return [

    ];
  }

}
