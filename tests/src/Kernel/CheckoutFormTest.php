<?php

namespace Drupal\Tests\commerce_postfinance_checkout\Kernel;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_postfinance_checkout\PluginForm\CheckoutForm;
use Drupal\commerce_postfinance_checkout\PostFinanceServiceFactory;
use Drupal\Core\Form\FormState;
use PostFinanceCheckout\Sdk\Model\Transaction;
use PostFinanceCheckout\Sdk\Service\TransactionPaymentPageService;
use PostFinanceCheckout\Sdk\Service\TransactionService;
use Prophecy\Argument;

/**
 * @coversDefaultClass \Drupal\commerce_postfinance_checkout\PluginForm\CheckoutForm
 *
 * @group commerce_postfinance_checkout
 */
class CheckoutFormTest extends CheckoutKernelTestBase {

  /**
   * @covers ::buildConfigurationForm
   */
  public function testBuildConfigurationForm() {
    $transaction = new Transaction();
    $transaction->setId(123);

    $transaction_service = $this->prophesize(TransactionService::class);
    $transaction_service
      ->create($this->spaceId, Argument::any())
      ->willReturn($transaction);

    $payment_page_service = $this->prophesize(TransactionPaymentPageService::class);
    $payment_page_service
      ->paymentPageUrl($this->spaceId, $transaction->getId())
      ->willReturn('https://redirect-url.postfinance.test');

    $service_factory = $this->prophesize(PostFinanceServiceFactory::class);
    $service_factory
      ->getTransactionService($this->userId, $this->secret)
      ->willReturn($transaction_service->reveal());

    $service_factory
      ->getTransactionPaymentPageService($this->userId, $this->secret)
      ->willReturn($payment_page_service->reveal());

    $this->container->set('commerce_postfinance_checkout.service_factory', $service_factory->reveal());
    $form_object = new CheckoutForm();

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = Payment::create([
      'state' => 'new',
      'amount' => $this->order->getTotalPrice(),
      'payment_gateway' => $this->paymentGateway->id(),
      'order_id' => $this->order->id(),
      'test' => TRUE,
      'authorized' => \Drupal::time()->getRequestTime(),
    ]);
    $payment->save();
    $form_object->setEntity($payment);

    $form = [];
    $form['#return_url'] = 'https://foo.bar.test/return';
    $form['#cancel_url'] = 'https://foo.bar.test/cancel';

    try {
      $form_object->buildConfigurationForm($form, new FormState());
      $this->fail('NeedsRedirectException was not thrown.');
    }
    catch (NeedsRedirectException $e) {
      /** @var \Drupal\Core\Routing\TrustedRedirectResponse $response */
      $response = $e->getResponse();
      $this->assertEquals('https://redirect-url.postfinance.test', $response->getTargetUrl());
    }

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $created_payment */
    $created_payment = reset($payments);

    $this->assertEquals('pending', $created_payment->getState()->value);
    $this->assertEquals('pending', $created_payment->getRemoteState());
    $this->assertEquals(123, $created_payment->getRemoteId());
    $this->assertEquals($this->order->getTotalPrice(), $created_payment->getAmount());
    $this->assertEquals('postfinance_checkout_id', $created_payment->getPaymentGatewayId());
  }

}
