<?php

namespace Drupal\Tests\commerce_postfinance_checkout\Kernel;

use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_postfinance_checkout\PostFinanceServiceFactory;
use Drupal\commerce_postfinance_checkout\Controller\WebhookController;
use Drupal\Component\Serialization\Json;
use PostFinanceCheckout\Sdk\Model\Transaction;
use PostFinanceCheckout\Sdk\Model\TransactionState;
use PostFinanceCheckout\Sdk\Service\TransactionService;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @coversDefaultClass \Drupal\commerce_postfinance_checkout\Controller\WebhookController
 *
 * @group commerce_postfinance_checkout
 */
class CheckoutWebhookTest extends CheckoutKernelTestBase {

  /**
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $transaction = new Transaction();
    $transaction->setId(1111);
    $transaction->setState(TransactionState::COMPLETED);

    $transaction_service = $this->prophesize(TransactionService::class);
    $transaction_service
      ->read($this->spaceId, Argument::any())
      ->willReturn($transaction);

    $service_factory = $this->prophesize(PostFinanceServiceFactory::class);
    $service_factory
      ->getTransactionService($this->userId, $this->secret)
      ->willReturn($transaction_service->reveal());

    $this->container->set('commerce_postfinance_checkout.service_factory', $service_factory->reveal());

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = Payment::create([
      'state' => 'pending',
      'amount' => $this->order->getTotalPrice(),
      'payment_gateway' => $this->paymentGateway->id(),
      'order_id' => $this->order->id(),
      'test' => TRUE,
      'authorized' => \Drupal::time()->getRequestTime(),
      'remote_id' => '1111'
    ]);
    $payment->save();
    $this->payment = $payment;
  }

  /**
   * @covers ::content
   */
  public function testOnReturnCompleteTransaction() {
    $webhook_data = [
      'listenerEntityTechnicalName' => 'Transaction',
      'entityId' => '1111',
      'spaceId' => $this->spaceId,
    ];

    // Create the request and call the notify method.
    $request = Request::create('', 'POST', [], [], [], [], Json::encode($webhook_data));

    $controller = WebhookController::create($this->container);
    $controller->content($request);

    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadMultiple(NULL);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $processed_payment */
    $processed_payment = reset($payments);

    $this->assertEquals('completed', $processed_payment->getState()->value);
    $this->assertEquals('completed', $processed_payment->getRemoteState());
    $this->assertEquals(1111, $processed_payment->getRemoteId());
    $this->assertEquals(\Drupal::time()->getRequestTime(), $processed_payment->getCompletedTime());
  }

  /**
   * @covers ::content
   */
  public function testOnReturnGatewayMismatch() {
    $this->payment->setRemoteId('0000');
    $this->payment->save();

    $webhook_data = [
      'listenerEntityTechnicalName' => 'Transaction',
      'entityId' => '1111',
      'spaceId' => '2222'
    ];

    // Create the request and call the notify method.
    $request = Request::create('', 'POST', [], [], [], [], Json::encode($webhook_data));
    $controller = WebhookController::create($this->container);
    try {
      $controller->content($request);
    }
    catch (\Exception $e) {
      $this->assertTrue($e instanceof AccessDeniedHttpException);
    }
  }

}
