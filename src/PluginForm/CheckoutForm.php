<?php

namespace Drupal\commerce_postfinance_checkout\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Redirect form for Checkout.
 */
class CheckoutForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState) {
    $form = parent::buildConfigurationForm($form, $formState);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();

    /** @var \Drupal\commerce_postfinance_checkout\Plugin\Commerce\PaymentGateway\PostfinanceCheckout $gateway */
    $gateway = $payment->getPaymentGateway()->getPlugin();

    try {
      // Get remote transaction to get the redirect url.
      $transaction = $gateway->createTransaction(
        $order,
        $payment,
        $form['#return_url'],
        $form['#cancel_url']
      );
    }
    catch (\Exception $e) {
      throw $gateway->mapException($e);
    }

    // Create payment.
    $payment->setState('pending');
    $payment->setRemoteState('pending');
    $payment->setRemoteId($transaction['transaction_id']);
    $payment->save();

    return $this->buildRedirectForm(
      $form,
      $formState,
      $transaction['redirect_url'],
      [],
      self::REDIRECT_GET
    );
  }

}
