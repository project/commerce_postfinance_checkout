<?php

namespace Drupal\commerce_postfinance_checkout;

use PostFinanceCheckout\Sdk\ApiClient;

/**
 * Handles the api services.
 */
class PostFinanceServiceFactory {

  /**
   * Create PostFinance PHP SDK Client.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   Secret.
   *
   * @return \PostFinanceCheckout\Sdk\ApiClient
   *   The post finance api client
   */
  private function apiClient($user_id, $secret) {
    return new ApiClient($user_id, $secret);
  }

  /**
   * Get the sdk transaction service.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   *
   * @return \PostFinanceCheckout\Sdk\Service\TransactionService
   *   The transaction service.
   */
  public function getTransactionService($user_id, $secret) {
    return $this->apiClient($user_id, $secret)->getTransactionService();
  }

  /**
   * Get the Transaction payment page service.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   *
   * @return \PostFinanceCheckout\Sdk\Service\TransactionPaymentPageService
   *   The transaction payment page service.
   */
  public function getTransactionPaymentPageService($user_id, $secret) {
    return $this->apiClient($user_id, $secret)->getTransactionPaymentPageService();
  }

  /**
   * Get the refund service.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   *
   * @return \PostFinanceCheckout\Sdk\Service\RefundService
   *   The refund service.
   */
  public function getRefundService($user_id, $secret) {
    return $this->apiClient($user_id, $secret)->getRefundService();
  }

  /**
   * Get the token service.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   *
   * @return \PostFinanceCheckout\Sdk\Service\TokenService
   *   The token service.
   */
  public function getTokenService($user_id, $secret) {
    return $this->apiClient($user_id, $secret)->getTokenService();
  }

  /**
   * Get the token version service.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   *
   * @return \PostFinanceCheckout\Sdk\Service\TokenVersionService
   *   The token version service.
   */
  public function getTokenVersionService($user_id, $secret) {
    // @todo In the previous version, we did not go through the method, but
    // created a new object ourselves.
    return $this->apiClient($user_id, $secret)->getTokenVersionService();
  }

}
