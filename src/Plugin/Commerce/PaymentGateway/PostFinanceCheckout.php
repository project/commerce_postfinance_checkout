<?php

namespace Drupal\commerce_postfinance_checkout\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_postfinance_checkout\PostFinanceServiceFactory;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\Entity\User;
use PostFinanceCheckout\Sdk\ApiClient;
use PostFinanceCheckout\Sdk\Model\AddressCreate;
use PostFinanceCheckout\Sdk\Model\EntityQuery;
use PostFinanceCheckout\Sdk\Model\LineItemCreate;
use PostFinanceCheckout\Sdk\Model\LineItemType;
use PostFinanceCheckout\Sdk\Model\Refund;
use PostFinanceCheckout\Sdk\Model\Token;
use PostFinanceCheckout\Sdk\Model\TokenizationMode;
use PostFinanceCheckout\Sdk\Model\TokenVersion;
use PostFinanceCheckout\Sdk\Model\Transaction;
use PostFinanceCheckout\Sdk\Model\TransactionCreate;
use PostFinanceCheckout\Sdk\Service\PaymentMethodConfigurationService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\Price;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Attribute\CommercePaymentGateway;
use Drupal\commerce_payment\Exception\AuthenticationException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_postfinance_checkout\PluginForm\CheckoutForm;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use PostFinanceCheckout\Sdk\ApiException;
use PostFinanceCheckout\Sdk\Model\ClientError;
use PostFinanceCheckout\Sdk\Model\Environment;

/**
 * Provides the PostFinance offsite checkout payment gateway.
 */
#[CommercePaymentGateway(
  id: 'postfinance_checkout',
  label: new TranslatableMarkup('PostFinance checkout'),
  display_label: new TranslatableMarkup('PostFinance'),
  modes: [
    Environment::PREVIEW => new TranslatableMarkup('Preview'),
    Environment::LIVE => new TranslatableMarkup('Live'),
  ],
  forms: [
    'offsite-payment' => CheckoutForm::class,
  ],
)]
class PostFinanceCheckout extends OffsitePaymentGatewayBase implements PostFinanceCheckoutInterface {

  /**
   * The post finance services factory.
   *
   * @var \Drupal\commerce_postfinance_checkout\PostFinanceServiceFactory
   */
  protected $postFinanceServicesFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->postFinanceServicesFactory = $container->get('commerce_postfinance_checkout.service_factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'space_id' => '',
        'user_id' => '',
        'secret' => '',
        'payment_configuration' => 'all',
        'payment_reusable' => 'ask',
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['space_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Space ID'),
      '#description' => $this->t('This is the space id from the PostFinance PHP SDK.'),
      '#default_value' => $this->configuration['space_id'],
      '#required' => TRUE,
    ];

    $form['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API User ID'),
      '#description' => $this->t('The user id from the PostFinance PHP SDK.'),
      '#default_value' => $this->configuration['user_id'],
      '#required' => TRUE,
    ];

    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Secret'),
      '#description' => $this->t('The secret from the PostFinance PHP SDK.'),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
    ];

    // Check if gateway is setup complete.
    if (isset($this->configuration['user_id']) AND $this->configuration['user_id'] != '') {
      $methods = [];
      $methods['all'] = 'All';

      // Call PostFinance API for payment methods.
      try {
        $client = new ApiClient($this->configuration['user_id'], $this->configuration['secret']);
        $configuration = new PaymentMethodConfigurationService($client);
        $query = new EntityQuery();
        $query->setLanguage('en');
        $configurations = $configuration->search($this->configuration['space_id'], $query);
        foreach ($configurations as $config) {
          $methods[$config->getId()] = $config->getName();
        }
      }
      catch (ApiException $e) {
        $this->messenger()->addError($this->t('Error on Communication with PostFinance, please check your settings.'));
      }

      if (count($methods) > 0) {
        // List all payment types they are possible from PostFinance.
        $form['payment_configuration'] = [
          '#type' => 'radios',
          '#options' => $methods,
          '#title' => $this->t('Payment Types'),
          '#description' => $this->t('If no selected we will use all.'),
          '#default_value' => $this->configuration['payment_configuration'],
        ];
      }

      // Allow user to save token.
      $form['payment_reusable'] = [
        '#type' => 'select',
        '#title' => $this->t('Request token and create reusable payment method'),
        '#description' => $this->t('Allows to reuse the payment information for additional payments. PostFinance allows right now only Visa, Master and Paypal.'),
        '#default_value' => $this->configuration['payment_reusable'],
        '#options' => [
          'always' => $this->t('Always, payment methods are created for logged in users only'),
          // @todo Remove if needed.
          'ask' => $this->t('Ask the user'),
          'never' => $this->t('Never'),
        ]
      ];

      // Webhook configuration info.
      $webhook_info = [];
      $webhook_info[] = '- Token | Name: "Token". States: "Active", "Inactive", "Deleting", "Deleted"';
      $webhook_info[] = '- Token Version | Name: "TokenVersion". States: "Active", "Uninitialized", "Obsolete"';
      $webhook_info[] = '- Transaction | Name: "Transaction". States: "Completed", "Failed"';

      $form['webhook_url_info'] = [
        '#markup' => '<b>' . $this->t('You need to setup following webhooks in PostFinance Backend') . '</b><br>' . implode('<br>', $webhook_info),
      ];

      $webhook_url = Url::fromRoute('commerce_postfinance_checkout.webhook')->setAbsolute()->toString();
      $form['webhook_url'] = [
        '#markup' => '<br>' . $this->t('The webhook url is %url', ['%url' => $webhook_url]) ,
      ];
    }
    else {
      $form['payment_configuration'] = [
        '#markup' => '<b>' . $this->t('After first saving, you can edit the settings again and set the payment methods you want.') . '</b>',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['space_id'] = $values['space_id'];
    $this->configuration['user_id'] = $values['user_id'];
    $this->configuration['secret'] = $values['secret'];
    $this->configuration['payment_reusable'] = ($values['payment_reusable'] ?? FALSE);
    $this->configuration['payment_configuration'] = ($values['payment_configuration'] ?? 'all');
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $paymentStorage->loadMultipleByOrder($order);

    $success = FALSE;
    foreach ($payments as $payment) {
      $transaction_id = $payment->getRemoteId();
      $transaction = $this->readTransaction($transaction_id);

      // Check state of transaction.
      // More infos https://app-postfinance.com/de-ch/doc/payment/transaction-process.
      if ($transaction->getState()) {
        // Transform postfinance state.
        $state = $this->transformTransactionState($transaction->getState());
        if ($state != NULL && $state != 'void') {
          $success = TRUE;
        }

        // Update payment.
        if ($state != NULL) {
          $payment->set('state', $state);
          $payment->set('remote_state', $state);
          $payment->save();
        }
      }

      // Check token on transaction.
      if ($transaction->getToken() instanceof Token) {
        // Anonymous cannot have tokens.
        if ($payment->getOrder()->uid->target_id != 0) {
          $remote_id = $transaction->getToken()->getId();
          $user_id = $payment->getOrder()->uid->target_id;
          $billing_profile = $order->getBillingProfile();
          $this->createPaymentMethod($remote_id, $user_id, $billing_profile);
        }
      }
    }

    if ($success == FALSE) {
      // Error on payment.
      throw new PaymentGatewayException('Payment incomplete or declined');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Validate the requested amount.
    $this->assertRefundAmount($payment, $amount);

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->state = 'partially_refunded';
    }
    else {
      $payment->state = 'refunded';
    }

    // Call remote refund on PostFinance.
    $refund = $this->createRefund($amount->getNumber(), $payment->getRemoteId());
    if ($refund->getState() == 'SUCCESSFUL') {
      // Save refund
      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();

      $this->messenger()->addStatus($this->t('Refund on payment %payment', [
        '%payment' => $payment->id(),
      ]));
    }
    else {
      // Error on refund.
      $this->messenger()->addError($this->t('Error refund on payment %payment', [
        '%payment' => $payment->id(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $order = $payment->getOrder();

    $token = $payment->payment_method->entity->remote_id->value;

    // Create transaction with no interaction.
    try {
      $transaction = $this->createTransactionNoUserInteraction($order, $payment, $token);
    }
    catch (\Exception $e) {
      throw $this->mapException($e);
    }

    // Error handling for transaction.
    if (!isset($transaction['id']) || $transaction['id'] == '') {
      $this->messenger()->addError($this->t('Error on payment gateway, no transaction found.'));
      throw new PaymentGatewayException('Error on payment gateway, no transaction found.');
    }

    if (!isset($transaction['state']) || $transaction['state'] == '') {
      $this->messenger()->addError($this->t('Error on payment gateway, no transaction found.'));
      throw new PaymentGatewayException('Error on payment gateway, no transaction found.');
    }

    if ($transaction['state'] == 'FAILED') {
      $message = 'Error on payment gateway, failure on transaction.';

      // Check if token is disabled.
      if (isset($transaction['token'])) {
        if ($transaction['token']->getState() == 'INACTIVE') {
          $message = 'Error on payment gateway, payment token is inactive.';
        }
      }

      $this->messenger()->addError($this->t($message));
      throw new PaymentGatewayException($message);
    }

    // Check state of transaction.
    // More infos https://app-postfinance.com/de-ch/doc/payment/transaction-process.
    // Transform postfinance state.
    $state = $this->transformTransactionState($transaction['state']);

    // Update payment.
    if ($state != NULL) {
      $payment->set('remote_id', $transaction['id']);
      $payment->set('state', $state);
      $payment->set('remote_state', $state);
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // @todo not supported atm.
  }

  /**
   * Create PostFinance line item.
   *
   * @param $order_id
   * @param $amount
   *
   * @return \PostFinanceCheckout\Sdk\Model\LineItemCreate
   */
  protected function createLineItem($order_id, $amount) {
    $lineItem = new LineItemCreate();
    $lineItem->setName('Order ' . $order_id);
    $lineItem->setUniqueId('orderid-' . $order_id);
    $lineItem->setQuantity(1);
    $lineItem->setAmountIncludingTax($amount);
    $lineItem->setType(LineItemType::FEE);

    return $lineItem;
  }

  /**
   * {@inheritdoc}
   */
  public function createAddress(ProfileInterface $profile, OrderInterface $order) {

    $postfinanceAddress = new AddressCreate();

    /** @var \Drupal\address\AddressInterface $address */
    $address = $profile->hasField('address') ? $profile->get('address')->first() : NULL;
    if ($address) {
      $postfinanceAddress->setOrganizationName($address->getOrganization());
      $postfinanceAddress->setFamilyName($address->getFamilyName());
      $postfinanceAddress->setGivenName($address->getGivenName());
      $postfinanceAddress->setStreet($address->getAddressLine1());
      $postfinanceAddress->setPostCode($address->getPostalCode());
      $postfinanceAddress->setCity($address->getLocality());
      $postfinanceAddress->setCountry($address->getCountryCode());
    }
    $postfinanceAddress->setEmailAddress($order->getEmail());

    \Drupal::moduleHandler()->alter('commerce_postfinance_checkout_address', $postfinanceAddress, $profile, $order);

    return $postfinanceAddress;
  }

  /**
   * {@inheritdoc}
   */
  public function createTransaction(OrderInterface $order, PaymentInterface $payment, $return_url, $cancel_url) {
    // Set user language.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $language_string = strtolower($language) . '-' . strtoupper($language);

    // Create addresses.
    $profiles = $order->collectProfiles();
    $billingAddress = NULL;
    if (isset($profiles['billing'])) {
      $billingAddress = $this->createAddress($profiles['billing'], $order);
    }

    // Create addresses.
    $shippingAddress = NULL;
    if (isset($profiles['shipping'])) {
      $shippingAddress = $this->createAddress($profiles['shipping'], $order);
    }

    $transactionPayload = new TransactionCreate();
    $transactionPayload->setMerchantReference($order->id());
    $transactionPayload->setLanguage($language_string);
    $transactionPayload->setSuccessUrl($return_url);
    $transactionPayload->setFailedUrl($cancel_url);
    $transactionPayload->setEnvironment($this->getMode());
    if ($billingAddress) {
      $transactionPayload->setBillingAddress($billingAddress);
    }
    if ($shippingAddress) {
      $transactionPayload->setShippingAddress($shippingAddress);
    }
    $transactionPayload->setCurrency($payment->getAmount()->getCurrencyCode());
    $transactionPayload->setLineItems([$this->createLineItem($order->id(), $payment->getAmount()->getNumber())]);
    $transactionPayload->setAutoConfirmationEnabled(TRUE);

    if (isset($this->configuration['payment_configuration']) && $this->configuration['payment_configuration'] != 'all') {
      $transactionPayload->setAllowedPaymentMethodConfigurations($this->configuration['payment_configuration']);
    }

    if ($this->shouldRequestToken($this->configuration)) {
      // Force token generation on postfinance.
      $transactionPayload->setTokenizationMode(TokenizationMode::FORCE_CREATION_WITH_ONE_CLICK_PAYMENT);
      $transactionPayload->setCustomerEmailAddress($transactionPayload->getBillingAddress()->getEmailAddress());
      if (\Drupal::currentUser()->isAuthenticated()) {
        $transactionPayload->setCustomerId(\Drupal::currentUser()->id());
      }
    }

    // Allow alter transaction over hook.
    \Drupal::moduleHandler()->alter('commerce_postfinance_checkout_transaction', $transactionPayload, $order);

    // Create transaction.
    $transaction_service = $this->postFinanceServicesFactory
      ->getTransactionService($this->configuration['user_id'], $this->configuration['secret']);
    $transaction = $transaction_service->create($this->configuration['space_id'], $transactionPayload);

    return [
      'transaction_id' => $transaction->getId(),
      'redirect_url' => $this->postFinanceServicesFactory
        ->getTransactionPaymentPageService($this->configuration['user_id'], $this->configuration['secret'])
        ->paymentPageUrl($this->configuration['space_id'], $transaction->getId()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createTransactionNoUserInteraction(OrderInterface $order, PaymentInterface $payment, $token) {
    // Create addresses.
    $profiles = $order->collectProfiles();
    $billingAddress = NULL;
    if (isset($profiles['billing'])) {
      $billingAddress = $this->createAddress($profiles['billing'], $order);
    }

    // Create addresses.
    $shippingAddress = NULL;
    if (isset($profiles['shipping'])) {
      $shippingAddress = $this->createAddress($profiles['shipping'], $order);
    }

    $transactionPayload = new TransactionCreate();
    $transactionPayload->setMerchantReference($order->id());
    $transactionPayload->setEnvironment($this->getMode());
    if ($billingAddress) {
      $transactionPayload->setBillingAddress($billingAddress);
    }
    if ($shippingAddress) {
      $transactionPayload->setShippingAddress($shippingAddress);
    }
    $transactionPayload->setCurrency($payment->getAmount()->getCurrencyCode());
    $transactionPayload->setLineItems([$this->createLineItem($order->id(), $payment->getAmount()->getNumber())]);
    $transactionPayload->setAutoConfirmationEnabled(TRUE);

    // Set token to transaction.
    $transactionPayload->setToken($token);

    // Create transaction.
    // @todo Test this.
    $transaction = $this->postFinanceServicesFactory
      ->getTransactionService($this->configuration['user_id'], $this->configuration['secret'])
      ->create($this->configuration['space_id'], $transactionPayload);
    $transaction_service = $this->postFinanceServicesFactory->getTransactionService($this->configuration['user_id'], $this->configuration['secret']);
    $transaction_return = $transaction_service->processWithoutUserInteraction($this->configuration['space_id'], $transaction->getId());

    return [
      'id' => $transaction_return->getId(),
      'authorization_amount' => $transaction_return->getAuthorizationAmount(),
      'authorization_environment' => $transaction_return->getAuthorizationEnvironment(),
      'authorized_on' => $transaction_return->getAuthorizedOn(),
      'completed_amount' => $transaction_return->getCompletedAmount(),
      'completed_on' => $transaction_return->getCompletedOn(),
      'currency' => $transaction_return->getCurrency(),
      'state' => $transaction_return->getState(),
      'token' => $transaction_return->getToken(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transformTransactionState($postfinance_state) {
    // State is authorized.
    $commerce_sate = NULL;
    if ($postfinance_state == 'VOIDED') {
      $commerce_sate = 'void';
    }
    if ($postfinance_state == 'AUTHORIZED') {
      $commerce_sate = 'authorize';
    }
    if ($postfinance_state == 'COMPLETED') {
      $commerce_sate = 'completed';
    }
    if ($postfinance_state == 'FULFILL') {
      $commerce_sate = 'completed';
    }

    return $commerce_sate;
  }

  /**
   * {@inheritdoc}
   */
  public function readTransaction($transaction_id) {
    $transaction = $this->postFinanceServicesFactory
      ->getTransactionService($this->configuration['user_id'], $this->configuration['secret'])
      ->read($this->configuration['space_id'], $transaction_id);

    return $transaction;
  }

  /**
   * {@inheritdoc}
   */
  public function createRefund($amount, $remote_id) {
    // Create refund object.
    $trans = new Transaction();
    $trans->setId($remote_id);

    $refund = new Refund();
    $refund->setAmount($amount);
    $refund->setExternalId(time());
    $refund->setMerchantReference('commerce_postfinance_checkout');
    $refund->setTransaction($trans);
    $refund->setType('MERCHANT_INITIATED_ONLINE');

    // Setup API client and call refund.
    return $this->postFinanceServicesFactory
      ->getRefundService($this->configuration['user_id'], $this->configuration['secret'])
      ->refund($this->configuration['space_id'], $refund);
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod($remote_id, $user_id, ProfileInterface $billing_profile) {
    // Owner.
    $owner = User::load($user_id);
    $owner_timezone = $owner->getTimeZone();

    // Get detail information from token version.
    $version = $this->getActiveTokenVersion($remote_id);
    if ($version instanceof TokenVersion) {
      $card_endingnumber = $version->getName();
      $expires_timestamp = NULL;
      if ($version->getExpiresOn() instanceof \DateTime) {
        $expires_timestamp = $version->getExpiresOn()->setTimezone(new \DateTimeZone($owner_timezone));
        $expires_timestamp = $expires_timestamp->getTimestamp();
      }

      // Transform credit card type.
      $card_type = $this->mapCardType($version->getPaymentConnectorConfiguration()->getName());
      if ($card_type == FALSE) {
        // Set a default value.
        $card_type = 'visa';
      }

      // Check if remote id exists.
      $payment_methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties([
        'remote_id' => $remote_id,
        'payment_gateway' => $this->parentEntity->id(),
      ]);
      $payment_method = NULL;
      if (count($payment_methods) > 0) {
        // Update existing.
        $payment_method = current($payment_methods);
        switch ($card_type) {
          case 'visa':
          case 'mastercard':
            $payment_method->expires->value = $expires_timestamp;
            $payment_method->card_type->value = $card_type;
            $payment_method->card_number->value = $card_endingnumber;
            $payment_method->card_exp_month->value = date('m', $expires_timestamp);
            $payment_method->card_exp_year->value = date('y', $expires_timestamp);
            break;
        }
      }
      else {
        // Create new one.
        $values = [];
        switch ($card_type) {
          case 'visa':
          case 'mastercard':
            $values = [
              'type' => 'credit_card',
              'payment_gateway' => $this->parentEntity,
              'remote_id' => $remote_id,
              'uid' => $user_id,
              'billing_profile' => $billing_profile,
              'reusable' => TRUE,
              'is_default' => TRUE,
              'expires' => $expires_timestamp,
              'card_type' => $card_type,
              'card_number' => $card_endingnumber,
              'card_exp_month' => date('m', $expires_timestamp),
              'card_exp_year' => date('y', $expires_timestamp),
            ];
            break;
          case 'paypal':
            // Extract mail address.
            $mail_address = '';
            $mail = mailparse_rfc822_parse_addresses($card_endingnumber);
            if (isset($mail[0]['address'])) {
              $mail_address = $mail[0]['address'];
            }

            $values = [
              'type' => 'paypal',
              'payment_gateway' => $this->parentEntity,
              'remote_id' => $remote_id,
              'uid' => $user_id,
              'billing_profile' => $billing_profile,
              'reusable' => TRUE,
              'is_default' => TRUE,
              'paypal_mail' => $mail_address,
            ];
            break;
        }

        if ($values) {
          $payment_method = $this->entityTypeManager->getStorage('commerce_payment_method')->create($values);
        }
      }
      if ($payment_method) {
        $payment_method->save();
      }
      return $payment_method;
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveTokenVersion($token_id) {
    $token_version_service = $this->postFinanceServicesFactory
      ->getTokenVersionService($this->configuration['user_id'], $this->configuration['secret']);
    return $token_version_service->activeVersion($this->configuration['space_id'], $token_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenVersion($token_version_id) {
    $token_version_service = $this->postFinanceServicesFactory
      ->getTokenVersionService($this->configuration['user_id'], $this->configuration['secret']);
    return $token_version_service->read($this->configuration['space_id'], $token_version_id);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $this->postFinanceServicesFactory
      ->getTokenVersion($this->configuration['user_id'], $this->configuration['secret'])
      ->delete($this->configuration['space_id'], $payment_method->getRemoteId());
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function webhookToken($data, PaymentMethodInterface $payment_method) {
    // Get token with entity id.
    $tokenService = $this->postFinanceServicesFactory
      ->getTokenService($this->configuration['user_id'], $this->configuration['secret']);
    $token = $tokenService->read($this->configuration['space_id'], $data['entityId']);
    if ($token instanceof Token) {
      // Check token state: DELETE.
      if ($token->getState() == 'DELETED') {
        $payment_method->delete();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function webhookTokenVersion($data, PaymentMethodInterface $payment_method) {
    // Get token version details.
    $tokenVersionService = $this->postFinanceServicesFactory
      ->getTokenVersionService($this->configuration['user_id'], $this->configuration['secret']);

    $version = $tokenVersionService->read($this->configuration['space_id'], $data['entityId']);
    if ($version instanceof TokenVersion) {
      $card_endingnumber = $version->getName();

      if ($version->getPaymentConnectorConfiguration() == NULL) {
        return FALSE;
      }

      $card_type = $this->mapCardType($version->getPaymentConnectorConfiguration()->getName());
      // Get token with entity id.
      if ($card_type == FALSE) {
        // Set a default value.
        $card_type = 'visa';
      }

      switch ($card_type) {
        case 'visa':
        case 'mastercard':
          $owner = $payment_method->get('uid')->entity;
          $owner_timezone = $owner->getTimeZone();
          $expires_timestamp = '';
          if ($version->getExpiresOn() != '') {
            $expires_timestamp = $version->getExpiresOn()->setTimezone(new \DateTimeZone($owner_timezone));
            $expires_timestamp = $expires_timestamp->getTimestamp();
          }

          $payment_method->expires->value = $expires_timestamp;
          $payment_method->card_type->value = $card_type;
          $payment_method->card_number->value = $card_endingnumber;
          $payment_method->card_exp_month->value = date('m', $expires_timestamp);
          $payment_method->card_exp_year->value = date('y', $expires_timestamp);
          break;
      }

      // Update existing token with remote information.
      $payment_method->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function webhookTransaction($data, PaymentInterface $payment) {
    $transaction = $this->readTransaction($data['entityId']);

    // Check state of transaction.
    // More infos https://app-postfinance.com/de-ch/doc/payment/transaction-process.
    if ($transaction->getState()) {
      // Transform postfinance state.
      $state = $this->transformTransactionState($transaction->getState());

      // Update payment.
      if ($state != NULL) {
        // This does not need to update the order, but use the loadForUpdate()
        // method of the order storage, this locking system requires
        // https://www.drupal.org/project/commerce/issues/3043180.
        $order_storage = $this->entityTypeManager->getStorage('commerce_order');
        if (\method_exists($order_storage, 'loadForUpdate')) {
          $order_storage->loadForUpdate($payment->getOrderId());
          $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

          // Reload the payment entity.
          $payment_storage->resetCache([$payment->id()]);
          $payment = $payment_storage->load($payment->id());
        }

        $payment->state->value = $state;
        $payment->remote_state->value = $state;
        $payment->completed->value = $this->time->getRequestTime();
        $payment->save();

        // The order storage does not have a public method to release the
        // lock. The current implementation uss the lock backend with
        // a given lock id.
        if (\method_exists($order_storage, 'loadForUpdate')) {
          $lock_id = 'commerce_order_update:' . $payment->getOrderId();
          \Drupal::lock()->release($lock_id);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function mapCardType($postfinance_card_type) {
    $card_type = '';
    if (strpos(strtolower($postfinance_card_type), 'visa') !== false) {
      $card_type = 'visa';
    }
    if (strpos(strtolower($postfinance_card_type), 'mastercard') !== false) {
      $card_type = 'mastercard';
    }
    if (strpos(strtolower($postfinance_card_type), 'paypal') !== false) {
      $card_type = 'paypal';
    }

    return $card_type;
  }

  /**
   * {@inheritdoc}
   */
  public function mapException(\Exception $exception) {
    $result = new PaymentGatewayException($exception->getMessage(), $exception->getCode(), $exception);

    if ($exception instanceof ApiException) {
      if ($exception->getCode() === 401) {
        $result = new AuthenticationException($exception->getMessage(), $exception->getCode(), $exception);
      }
      elseif ($exception->getResponseObject() instanceof ClientError) {
        $result = new InvalidRequestException($exception->getMessage(), $exception->getCode(), $exception);
      }
    }

    return $result;
  }

  /**
   * Returns whether a token should be rquested.
   *
   * @param array $plugin_configuration
   *   The payment gateway plugin configuration.
   *
   * @return bool
   *   TRUE if a token should be requested.
   */
  protected function shouldRequestToken(array $plugin_configuration) {
    if ($plugin_configuration['payment_reusable'] == 'always') {
      return TRUE;
    }

    if ($plugin_configuration['payment_reusable'] == 'ask' && \Drupal::currentUser()->id() != 0) {
      // Get user temp store.
      $tempstore = \Drupal::service('tempstore.private');
      $store = $tempstore->get('commerce_postfinance_checkout');
      return $store->get('save_payment', FALSE);
    }
  }

}
