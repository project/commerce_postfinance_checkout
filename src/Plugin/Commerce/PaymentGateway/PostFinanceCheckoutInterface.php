<?php

namespace Drupal\commerce_postfinance_checkout\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsUpdatingStoredPaymentMethodsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 *  Extends with methods related to Checkout PostFinance functionality.
 */
interface PostFinanceCheckoutInterface extends OffsitePaymentGatewayInterface, SupportsStoredPaymentMethodsInterface, SupportsUpdatingStoredPaymentMethodsInterface, SupportsRefundsInterface {

  /**
   * Creates a PostFinance address from a profile and order.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Postfinance\Sdk\Model\Address
   *   The address object.
   */
  public function createAddress(ProfileInterface $profile, OrderInterface $order);

  /**
   * Read transaction.
   *
   * @param $transaction_id
   *
   * @return \PostFinanceCheckout\Sdk\Model\Transaction
   * @throws \PostFinanceCheckout\Sdk\ApiException
   * @throws \PostFinanceCheckout\Sdk\Http\ConnectionException
   * @throws \PostFinanceCheckout\Sdk\VersioningException
   */
  public function readTransaction(string $transaction_id);

  /**
   * Create transaction.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param $return_url
   * @param $cancel_url
   *
   * @return array
   * @throws \PostFinanceCheckout\Sdk\ApiException
   * @throws \PostFinanceCheckout\Sdk\Http\ConnectionException
   * @throws \PostFinanceCheckout\Sdk\VersioningException
   */
  public function createTransaction(OrderInterface $order, PaymentInterface $payment, $return_url, $cancel_url);

  /**
   * Create transaction without user interraction.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param $token
   *
   * @return array
   *
   * @throws \PostFinanceCheckout\Sdk\ApiException
   * @throws \PostFinanceCheckout\Sdk\Http\ConnectionException
   * @throws \PostFinanceCheckout\Sdk\VersioningException
   */
  public function createTransactionNoUserInteraction(OrderInterface $order, PaymentInterface  $payment, $token);

  /**
   * Transform PostFinance transaction state to drupal commerce state.
   *
   * @param $postfinance_state
   *
   * @return string|null
   */
  public function transformTransactionState($postfinance_state);

  /**
   * Create refund for transaction.
   *
   * @param $amount
   * @param $remote_id
   *
   * @return mixed
   */
  public function createRefund($amount, $remote_id);

  /**
   * Create Payment method with user data.
   *
   * @param $remote_id
   * @param $user_id
   * @param \Drupal\profile\Entity\ProfileInterface $billing_profile
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]|false|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \PostFinanceCheckout\Sdk\ApiException
   * @throws \PostFinanceCheckout\Sdk\Http\ConnectionException
   * @throws \PostFinanceCheckout\Sdk\VersioningException
   */
  public function createPaymentMethod($remote_id, $user_id, ProfileInterface $billing_profile);

  /**
   * Webhook for Token update.
   *
   * @param $data
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \PostFinanceCheckout\Sdk\ApiException
   * @throws \PostFinanceCheckout\Sdk\Http\ConnectionException
   * @throws \PostFinanceCheckout\Sdk\VersioningException
   */
  public function webhookToken($data, PaymentMethodInterface $payment_method);

  /**
   * Webhook for Token Version update.
   *
   * @param $data
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \PostFinanceCheckout\Sdk\ApiException
   * @throws \PostFinanceCheckout\Sdk\Http\ConnectionException
   * @throws \PostFinanceCheckout\Sdk\VersioningException
   */
  public function webhookTokenVersion($data, PaymentMethodInterface $payment_method);

  /**
   * Webhook for Transaction update.
   *
   * @param $data
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function webhookTransaction($data, PaymentInterface $payment);

  /**
   * Maps the credit card type to a Commerce credit card type.
   *
   * @param $card_type
   *
   * @return mixed|string
   */
  public function mapCardType($postfinance_card_type);

  /**
   * Maps a PostFinanceCheckout exception to a Commerce Payment Exception.
   *
   * @param \Exception $exception
   *   A PostFinanceCheckout exception.
   *
   * @return \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   A commerce payment exception.
   */
  public function mapException(\Exception $exception);

  /**
   * Get the active token version.
   *
   * @param int $token_id
   *   The Token id.
   *
   * @return \PostFinanceCheckout\Sdk\Model\TokenVersion
   *   The token version object.
   */
  public function getActiveTokenVersion($token_id);


  /**
   * Get the token version.
   *
   * @param int $token_version_id
   *   The Token version id.
   *
   * @return \PostFinanceCheckout\Sdk\Model\TokenVersion
   *   The token version object.
   */
  public function getTokenVersion($token_version_id);
}
