<?php

namespace Drupal\commerce_postfinance_checkout\Controller;

use Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface;
use Drupal\commerce_postfinance_checkout\Plugin\Commerce\PaymentGateway\PostFinanceCheckout;
use Drupal\commerce_postfinance_checkout\PostFinanceServiceFactory;
use Drupal\Core\Controller\ControllerBase;
use PostFinanceCheckout\Sdk\Model\TokenVersion;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Deals with webhook calls.
 */
class WebhookController extends ControllerBase {

  /**
   * Handles the webhook request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   The response for PostFinance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function content(Request $request) {
    // Get data from webhook.
    $data_post = $request->getContent();

    // Check if webhook URL is in use.
    if (!$data_post) {
      throw new AccessDeniedHttpException('No payload');
    }

    $data = json_decode($data_post, TRUE);
    if (isset($data['listenerEntityTechnicalName']) && isset($data['entityId']) && isset($data['spaceId'])) {

      // Get the relevant local entity based on the remote id.
      switch ($data['listenerEntityTechnicalName']) {
        case 'TokenVersion';
          $gateway_candidates = $this->entityTypeManager()
            ->getStorage('commerce_payment_gateway')
            ->loadByProperties([
              'configuration.space_id' => $data['spaceId'],
              'plugin' => 'postfinance_checkout',
            ]);

          // It is possible to have multiple gateways with the same space id,
          // that should not be a problem as this is just needed to get the
          // token.
          /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $gateway */
          $gateway = reset($gateway_candidates);
          if (empty($gateway)) {
            throw new AccessDeniedHttpException('Gateway mismatch.');
          }

          /** @var \Drupal\commerce_postfinance_checkout\Plugin\Commerce\PaymentGateway\PostFinanceCheckoutInterface $gateway_plugin */
          $gateway_plugin = $gateway->getPlugin();

          $version = $gateway_plugin->getTokenVersion($data['entityId']);

          if ($version instanceof TokenVersion == FALSE) {
            throw new AccessDeniedHttpException('No active version.');
          }

          if ($version->getState() == 'OBSOLETE') {
            break;
          }

          $entity = $this->loadLocalEntity($data['listenerEntityTechnicalName'], $version->getToken()->getId());

          // We get the gateway again, this time from the plugin, to be extra
          // sure that we have the right one.
          $gateway_plugin = $entity->getPaymentGateway()->getPlugin();
          $gateway_plugin->webhookTokenVersion($data, $entity);
          break;

        case 'Token':
          $entity = $this->loadLocalEntity($data['listenerEntityTechnicalName'], $data['entityId']);
          $gateway_plugin = $entity->getPaymentGateway()->getPlugin();
          $gateway_plugin->webhookToken($data, $entity);
          break;

        case 'Transaction':
          $entity = $this->loadLocalEntity($data['listenerEntityTechnicalName'], $data['entityId']);
          $gateway_plugin = $entity->getPaymentGateway()->getPlugin();
          $gateway_plugin->webhookTransaction($data, $entity);
          break;
      }
    }
    else {
      throw new AccessDeniedHttpException('Missing parameters.');
    }

    $response = new Response();
    $response->setContent('Ok');
    return $response;
  }

  /**
   * Loads the local entity that matches the remote id.
   *
   * @param string $listener_name
   *   The listener technical name as specified in PostFinance backoffice.
   * @param int $remote_id
   *   The remote id.
   *
   * @return \Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface
   *   The local entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadLocalEntity(string $listener_name, int $remote_id): EntityWithPaymentGatewayInterface {
    $entity_type = ($listener_name == 'Transaction') ? 'commerce_payment' : 'commerce_payment_method';
    /** @var \Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface[] $entity_candidates */
    $entity_candidates = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->loadByProperties(['remote_id' => $remote_id]);

    foreach ($entity_candidates as $entity) {
      if ($entity->getPaymentGateway()->getPlugin() instanceof PostFinanceCheckout) {
        return $entity;
      }
    }

    throw new AccessDeniedHttpException('No payment methods found.');
  }

}
